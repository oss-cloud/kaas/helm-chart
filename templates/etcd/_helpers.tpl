{{- define "etcd.cluster" -}}
{{- $initialCluster := list -}}
{{- $replicas := int .Values.etcd.replicas -}}
{{- range $e, $i := until $replicas -}}
{{- $instance := printf "%s-etcd-%d" $.Release.Name $i -}}
{{- $serviceName := printf "%s=https://%s.%s-etcd.%s.svc.cluster.local:2380" $instance  $instance $.Release.Name $.Release.Namespace -}}
{{- $initialCluster = append $initialCluster $serviceName -}}
{{- end -}}
{{- join "," $initialCluster | quote -}}
{{- end -}}